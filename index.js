// JS comments are important, it acts as a guide for the programmer. It is also disregarded no matter how long it may be.

/* 
    This
    is
    a
    multi
    line
    comment
*/

// STATEMENTS - are instructions that we tell the computer to perform. It usually ends with a semi-colon.

// SYNTAX - are set of rules that describes how statement must be constructed. 

// This is a statement with a correct syntax.
console.log("Hello World!");

// JS is a loose typed programming language. 
console. log("  Hello World ");

// And also with this:
console.
log
(
"Hello Again"
)

// [SECTION] Variables - It is used as a container or storage.

// Declaring variables - tells the devices that a variable name is created and is ready to store data. 
// Syntax - let/const variableName;

// "let" is a keyword that is usually used to declare a varaible.
let myVariable;
let hello;

console.log(myVariable);
console.log(hello);


// Guidelines for declaring a variable
/* 
    1. Use "let" keyword followed by a variable name, and then followed again by the assignment(=).
    2. Variable names should start with a lowercase letter.
    3. For constant variable we are using "const" keyword.
    4. Variable names should be comprehensive or descriptive. 
    5. Don't use reserved keywords as a variable name.
*/

// = sign stands for initialization, that means giving a value to a variable.

/* 

        camel case - thisIsCammelCasing
        snake case - this_is_snake_casing
        kebab case - this-is-kebab-casing
*/

// This is a good variable name.
let firstName = "Michael";

// This is a bad variable name.
let pokemon = 25000;


// Declaring and initializing variables.
// Syntax -> let/const variableName = value;
let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

// re-assigning a value to a variable.
productPrice = 25000;
console.log(productPrice);
console.log(productPrice);
console.log(productPrice);

let friend = "Kate";
friend = "Jane";
console.log(friend);

let supplier;
supplier = "John Smith Tradings";
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

// let vs const
// "let" variables values can be changed, we can re-assigned new values.
// "const" varaible values cannot be changed.

const hoursInADay = 24;
console.log(hoursInADay);

const pi = 3.14;
console.log(pi);


// local/global scope variables.
let outerVariable = "Hi!";  // This is a global variable.
{
    let innerVariable = "Hello Again"; // This is a local variable.
}

console.log(outerVariable);
// console.log(innerVariable); <- This will throw an error since it is enclosed with curly braces.

// Multiple variable declaration can be done in one line.
let productCode = "DC-017", productBrand = "Dell";
console.log(productCode, productBrand);


// Strings = are series of characters that creates a word.
let country = "Philippines";
let province = "Metro Manila";

// Concatenation of strings. use "+" symbol.
let fullAddress = province + " " + country;
console.log(fullAddress);

// Escape Character (\)
// "\n" refers to creating a new line between text.
let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

let message = "John's employee went home early.";
console.log(message);
message = 'John\'s employee went home early.';
console.log(message);

// Numbers are integers or whole numbers.
let headcount = 26;
console.log(headcount);

// Decimal or Fractions.
let grade = 98.7;
console.log(grade);

// Exponential 
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and strings.
console.log("Johnn grade last quarter is " + grade);

// Boolean
let isMarried = false;
let isGoodConduct = true;

console.log("Is Married: " + isMarried + "\nIs Good Conduct: " + isGoodConduct);

// Arrays are special kind of data type. It is used to store multiple values with the same data types.
// Syntax -> let/const arrayName = [];
let grades = [98.7, 92.1, 90.2];
console.log(grades);

let details = ["Jon", "Smith", 32, true];
console.log(details);

// Objects - holds properties that describes the variable.
// Syntax -> let/const objectName = { propertyA = value, propertyB = value };
let person = {
    fullName : "Juan dela Cruz",
    age: 35,
    contact: ["012345689", "9874561230"],
    address: {
        houseNumber: "345",
        city: "Manila",
    }
};
console.log(person);

let myGrades = {
    firstGrading: 98.7,
    secondGrading: 92.1,
    thirdGrading: 90.2,
    fourthGrading: 94.6
};
console.log(myGrades);

// Checking of data type.
console.log(typeof(grades));
console.log(typeof(person));
console.log(typeof(isMarried));

const anime = ["One Piece", "One Punch Man", "Attack On Titan"];
// anime = ["Kimetsu No Yaiba"]; <- This will generate an error.
anime[0] = "Kimetsu No Yaiba";
console.log(anime);

// null vs undefined
// null when a variable has 0 or empty value. undefined when a variable has no value upon declaration.